gsap.to(".skills", {
    opacity:1,
    duration:0.5,
    scrollTrigger: {
        trigger:'#section2',
        start:'bottom 900',
    }
})

gsap.from(".skills", {
    y:120,
    duration:1,
    ease: 'elastic',
    scrollTrigger: {
        trigger:'#section2',
        start: 'bottom 900',
        
    }
})
